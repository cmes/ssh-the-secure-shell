# What Is SSH?
SH, the Secure Shell, is a popular, powerful, software-based approach to network
security.

To log into an account with the username smith on the remote computer host.example.com, use this command:
```
$ ssh -l root 192.168.32.131
```

Using SSH, the file can be transferred securely between machines with a single secure
copy command. If the file were named myfile, the command executed on
firstaccount.com might be:
```
$ scp sample.txt root@192.168.32.131:/root
$ scp foobar.txt your_username@remotehost.edu:/some/remote/directory
$ scp your_username@remotehost.edu:foobar.txt /some/local/directory
```

# Basic Client Use

## verbose mode for more log 
```
$ ssh -v -l root 192.168.32.131
```

## To change the ssh escape character
```
$ ssh -e "%" -l root 192.168.32.131
```

### Generating Key Pairs with ssh-keygen (/root/.ssh/id_dsa)
```
$ ssh-keygen -t dsa
```
### Installing a Public Key on an SSH Server Machine
Create or edit the remote file ~/.ssh/authorized_keys and append your public key 
the contents of the id_dsa.pub file you generated on the local machine
```
$ mkdir .ssh
$ cd .ssh/
$ cat > authorized_keys
$ paste the public key
```
Regardless of which SSH implementation you use, make sure your remote SSH directory and associated files are writable only by your account:*
```
$ chmod 755 ~/.ssh
$ chmod 644 ~/.ssh/authorized_keys
```

You are now ready to use your new key to access the pat account:
```
$ ssh -l root 192.168.32.131
```

Installing OpenSSH Keys with ssh-copy-id
```
$ ssh-copy-id -i mykey dulaney@server.example.com
```

#2.5 The SSH Agent
```
TODO
```